package com.kshrd.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NextFragment nextFragment = new NextFragment();
        MainFragment mainFragment = new MainFragment();

        if (Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
            getSupportFragmentManager().beginTransaction().replace(R.id.mMainFragment, new MainFragment()).commit();
        } else if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            getSupportFragmentManager().beginTransaction().replace(R.id.layoutLeft, mainFragment).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.layoutRight, nextFragment).commit();
        }
    }


}